﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {

	public GameObject shot;

	public float coolDown = 0.3f;
	private float _timer = 0;

	public float lifeSpan = 1f;

	private CameraFollow cameraFollow;

	void Start(){
		cameraFollow = Camera.main.GetComponent<CameraFollow>();
	}


	// Update is called once per frame
	void Update () {
		/*
		if(Input.GetMouseButtonDown(0)){
			Shot();
		}*/
		if(Input.GetMouseButton(0)){
			if(_timer == 0){
				Shot ();
				_timer = coolDown;
			}
		}
		else _timer = 0;

		if(_timer > 0){
			_timer -= Time.deltaTime;
			if(_timer < 0) _timer = 0;
		}
	}

	void Shot(){

		Vector3 startPos = transform.position + transform.rotation * Vector3.forward * 3f;
		Quaternion rotation = transform.rotation;

		Vector3 camOffset = Camera.main.transform.position - transform.position;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + cameraFollow.offset);
		Vector3 point = ray.GetPoint(1000f);

		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 1000f, 0)){
			point = hit.point;
		}

		rotation.SetLookRotation(point - startPos - camOffset);


		GameObject go = GameObject.Instantiate(shot, startPos, rotation) as GameObject;
		go.GetComponent<Rigidbody>().AddForce(GetComponent<Rigidbody>().velocity + go.transform.forward * 200f, ForceMode.Impulse);
		GameObject.Destroy(go, lifeSpan);

		/*
		Vector3 startPos = transform.position + transform.rotation * Vector3.forward * 30f;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit hit;

		Vector3 point = ray.GetPoint(100f);


		Quaternion rotation = Quaternion.LookRotation(point - startPos);
		Debug.Log(rotation);

		//GameObject inst = GameObject.Instantiate(shot, transform.position + transform.rotation * Vector3.forward * 3f, transform.rotation) as GameObject;
		GameObject inst = GameObject.Instantiate(shot, startPos, rotation) as GameObject;
		GameObject.Destroy(inst, lifeSpan);*/

		/*
		Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 30f);
		position = Camera.main.ScreenToWorldPoint(position);
		//Vector3 tr = Camera.main. 
		GameObject go = GameObject.Instantiate(shot, transform.position, Quaternion.identity) as GameObject;
		go.transform.LookAt(position - transform.position); 

		GameObject.Destroy(go, lifeSpan);*/
	}
}




















