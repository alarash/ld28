﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogLine {
	public float delay = 0;
	public float pause = 3;

	[Multiline]
	public string text = "";


	public DialogLine(string text, float delay, float pause){
		this.text = text;
		this.delay = delay;
		this.pause = pause;
	}
	
}

public class DialogScript: MonoBehaviour {
	public GUISkin skin;

	public DialogLine[] dialogs;

	public float readSpeed = 5f;
	public Texture defaultPortrait;

	private int currentLine = 0;
	private int currentChar = 0;

	//private int delayTimer = 0;
	//private int pauseTimer = 0;

	private bool showGUI = false;

	public bool skippable = true;
	private bool _skipped = false;

	/*
	void Update(){
		if(currentLine >= dialogs.Length) return;


	}*/

	void Start(){
		StartDialog();
	}

	void Update(){
		if(skippable && !_skipped && Input.GetKeyDown(KeyCode.Space)){
			Debug.Log("Skip");
			_skipped = true;
			Hide ();

			SendMessage("OnDialogSkip", SendMessageOptions.DontRequireReceiver);
		}
	}

	IEnumerator DialogCoroutine(){
		while(true){
			if(currentLine >= dialogs.Length){
				Hide ();
				yield break;
			}

			DialogLine current = dialogs[currentLine];

			//DELAY
			if(current.delay > 0)
				yield return new WaitForSeconds(current.delay);

			//SHOW
			GetComponent<AudioSource>().Play();
			showGUI = true;

			for(int i = 0; i <= current.text.Length; i++){
				currentChar = i;
				yield return new WaitForSeconds(1f / readSpeed);
			}
			
			//Debug.Log(current.text);

			//PAUSE
			if(current.pause > 0)
				yield return new WaitForSeconds(current.pause);
			//Debug.Log("End");

			showGUI = false;

			//NEXT
			currentLine++;
			yield return null;
		}
	}

	void OnGUI(){
		if(!showGUI) return;

		DialogLine current = dialogs[currentLine];
		//string txt = current.text;

		string txt = current.text.Substring(0, (int)currentChar);

		if(defaultPortrait != null){
			Rect portraitRect = new Rect(0, 0, defaultPortrait.width * 0.7f, defaultPortrait.height * 0.7f);
			portraitRect.x = Screen.width - portraitRect.width - 10;
			portraitRect.y = Screen.height - portraitRect.height - 95;


			GUI.DrawTexture(portraitRect, defaultPortrait);
		}

		GUI.Label (new Rect(0, Screen.height - 100, Screen.width, 100), txt, skin.GetStyle("Dialog"));

		if(skippable)
			GUI.Label(new Rect(Screen.width - 180, Screen.height - 40, 180, 40), "[Space] to Skip", skin.GetStyle("DialogSmall"));
	}

	public void StartDialog(){
		currentLine = 0;
		currentChar = 0;

		StopCoroutine("DialogCoroutine");
		StartCoroutine("DialogCoroutine");
		//StartCoroutine(DialogCoroutine());
	}

	public void Hide(){
		currentLine = 0;
		currentChar = 0;
		StopCoroutine("DialogCoroutine");

		showGUI = false;
		GetComponent<AudioSource>().Stop();
	}
}







public class DialogScriptOld : MonoBehaviour {

	public GUISkin skin;

	//public float delay = 2f;
	private float delayTimer = 0f;

	public float readSpeed = 5f;

	//public string[] dialogs;
	public DialogLine[] dialogs;

	private int currentLine = 0;
	private float currentChar = 0;

	//public float pauseLine = 1f;
	private float pauseTimer = 0f;

	// Use this for initialization
	void Start () {
		//audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentLine >= dialogs.Length) return;

		/*if(currentLine == -1){
			currentLine ++;
			audio.Play();
		}*/

		if(delayTimer < dialogs[currentLine].delay){
			delayTimer += Time.deltaTime;
			return;
		}
		else {
			GetComponent<AudioSource>().Play();
		}

		if(currentChar < dialogs[currentLine].text.Length)
			currentChar += Time.deltaTime * readSpeed;
		else {
			pauseTimer += Time.deltaTime;

			if(pauseTimer > dialogs[currentLine].pause/*pauseLine*/){
				currentLine++;
				currentChar = 0;
				pauseTimer = 0;

				delayTimer = 0;

				if(currentLine < dialogs.Length)
					GetComponent<AudioSource>().Play();
			}
		}


	}

	public void StartDialog(){
		currentLine = -1;
		currentChar = 0;
		pauseTimer = 0;
	}

	void OnGUI(){
		if(currentLine >= dialogs.Length) return;

		if(delayTimer < dialogs[currentLine].delay) return;
		if(currentLine < 0 || currentLine >= dialogs.Length) return;

		GUI.skin = skin;

		//string txt = dialogs[currentLine].Substring(0, Mathf.Min(dialogs[currentLine].Length, (int)currentChar));
		string txt = dialogs[currentLine].text.Substring(0, Mathf.Min(dialogs[currentLine].text.Length, (int)currentChar));

		txt = txt.Replace("|", "\n");

		GUI.Label (new Rect(0, Screen.height - 100, Screen.width, 100), txt, skin.GetStyle("Dialog"));
	}
}
