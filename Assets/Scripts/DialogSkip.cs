﻿using UnityEngine;
using System.Collections;

public class DialogSkip : MonoBehaviour {

	/*
	private DialogScript dialog;

	private bool _skipped = false;

	// Use this for initialization
	void Start () {
		dialog = GetComponent<DialogScript>(); 
	}
	
	void OnGUI(){
		if(_skipped) return;

		GUI.skin = dialog.skin;
		GUI.Label(new Rect(Screen.width - 180, Screen.height - 40, 180, 40), "[Space] to Skip", GUI.skin.GetStyle("DialogSmall"));
	}

	void Update(){
		if(!_skipped && Input.GetKeyDown(KeyCode.Space)){
			Debug.Log("Skip");
			_skipped = true;
			Skip();
		}
	}

	void Skip(){
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.transform.position = new Vector3(0, 0, -150f);

		//dialog.enabled = false;
		dialog.Hide();

		enabled = false;
	}*/

	void OnDialogSkip(){
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if(player.transform.position.z < -150f)
			player.transform.position = new Vector3(0, 0, -150f);
	}
}
