﻿using UnityEngine;
using System.Collections;

public class Mouse : MonoBehaviour {

	public Camera cam;

	private Health health;

	private CameraFollow cameraFollow;

	// Use this for initialization
	void Start () {
		health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();

		cameraFollow = Camera.main.GetComponent<CameraFollow>();
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.instance.IsPaused()) return;
		if(health.health <= 0){
			Cursor.visible = true;
			gameObject.SetActive(false);
			return;
		}

		Cursor.visible = false;

		//transform.position = Input.mousePosition + Vector3.forward * 5f;
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		//transform.position = ray.GetPoint(15f) + cameraFollow.offset;// - Camera.main.transform.position;
		transform.position = ray.GetPoint(15f);// + cameraFollow.offset;

		transform.Rotate(0, 0, Time.deltaTime * 200f);
	}
}
