﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	public GUISkin skin;

	public DialogScript dialog;

	// Use this for initialization
	void Start () {
		StartCoroutine(StartDialog());

	}

	IEnumerator StartDialog(){
		yield return new WaitForSeconds(10f);

		dialog.skippable = false;

		dialog.dialogs = new DialogLine[2];//new string[2];
		//dialog.dialogs[0].text = "Dammit Bob we've lost another one...";
		//dialog.dialogs[1].text = "To the clonning machine !!!";
		dialog.dialogs[0] = new DialogLine("Dammit Bob we've lost another one...", 0, 3);
		dialog.dialogs[1] = new DialogLine("To the clonning machine !!!", 1, 5);
		
		dialog.StartDialog();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.skin = skin;

		Cursor.visible = true;
			
		Rect rect = new Rect(0, 0, 200, 100);
		rect.x = (Screen.width - rect.width) * 0.5f;
		rect.y = (Screen.height - rect.height) * 0.5f + 150f;
		
		//GUI.BeginGroup(rect, "", GUI.skin.box);
		GUILayout.BeginArea(rect, "");
		//GUILayout.Space(20);
		if(GUILayout.Button("Retry")) Application.LoadLevel(Application.loadedLevel);
		if(GUILayout.Button("Title Screen")) Application.LoadLevel(0);
		
		
		GUILayout.EndArea();
			

	}
}
